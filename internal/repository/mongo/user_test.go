package mongo

import (
	"context"
	"testing"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"gitlab.com/exmatic/auth/internal/core/role"
	"gitlab.com/exmatic/auth/internal/core/user"
)

func database() (*mongo.Database, error) {
	ctx := context.Background()
	clientOpts := options.Client().ApplyURI("mongodb://localhost:27017")
	client, err := mongo.Connect(ctx, clientOpts)
	if err != nil {
		return nil, err
	}
	return client.Database("exmatic-auth"), nil
}

func TestUserRepository_Create(t *testing.T) {
	ctx := context.Background()
	db, err := database()
	if err != nil {
		t.Fatalf("error : %v", err)
	}

	repo := NewUserRepository(db)

	now := time.Now()
	model := &user.Model{
		ID: primitive.ObjectID{},
		Profile: &user.ProfileModel{
			Email:     wrap("aybarrel@gmail.com"),
			Username:  wrap("aybarrel"),
			Firstname: wrap("Aybar"),
			Lastname:  wrap("Zholamanov"),
			BirthDate: &now,
			Password:  wrap("fuckyou123"),
		},
		Info:      nil,
		Role:      role.User,
		CreatedAt: &now,
		UpdatedAt: &now,
	}

	if _, err := repo.Create(ctx, model); err != nil {
		t.Fatalf("error : %v", err)
	}
}

func TestUserRepository_Update(t *testing.T) {
	ctx := context.Background()
	db, err := database()
	if err != nil {
		t.Fatalf("error : %v", err)
	}

	repo := NewUserRepository(db)
	//birthDate := time.Date(2002, 3, 8, 0, 0, 0, 0, &time.Location{})

	id, err := primitive.ObjectIDFromHex("6261f7a01452fcbd2466f0fe")
	if err != nil {
		t.Fatalf("error : %v", err)
	}
	model := &user.Model{
		ID: id,
		//Email:		  wrap("altercolt@gmail.com"),
		//Username:     wrap("altercolt"),
		//Firstname:    wrap("Van"),
		//Position:     wrap("Lead Rust Developer"),
		//BirthDate:    &birthDate,
		//Lastname:     wrap("Altercolt"),
	}

	err = repo.Update(ctx, model)
	if err != nil {
		t.Fatalf("err : %v", err)
	}

}

func TestUserRepository_FetchOne(t *testing.T) {
	ctx := context.Background()
	db, err := database()
	if err != nil {
		t.Fatalf("error : %v", err)
	}

	repo := NewUserRepository(db)

	username := "aybarrel@gmail.com"

	res, err := repo.FetchOne(ctx, bson.D{{
		"$or", bson.A{
			bson.D{{"profile.username", username}},
			bson.D{{"profile.email", username}},
		},
	}})

	if err != nil {
		t.Fatalf("err : %v", err)
	}

	t.Log(*res)
}

func TestUserRepository_Fetch(t *testing.T) {

}

func TestUserRepository_Delete(t *testing.T) {

}

func wrap(in string) *string {
	return &in
}
