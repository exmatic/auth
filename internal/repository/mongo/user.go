package mongo

import (
	"context"
	"fmt"

	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/exmatic/auth/internal/core/user"
)

type UserRepository struct {
	db *mongo.Collection
}

func NewUserRepository(db *mongo.Database) user.Repository {
	coll := db.Collection("users")
	return &UserRepository{
		db: coll,
	}
}

func (r *UserRepository) FetchOne(ctx context.Context, id bson.D) (*user.User, error) {
	res := r.db.FindOne(ctx, id)
	if res.Err() != nil {
		return nil, fmt.Errorf("fetching single user : %w", res.Err())
	}

	var u user.Model

	if err := res.Decode(&u); err != nil {
		return nil, fmt.Errorf("fetching single user : %w", err)
	}

	return u.User(), nil
}

func (r *UserRepository) Fetch(ctx context.Context, filters bson.D) ([]*user.User, error) {
	cursor, err := r.db.Find(ctx, filters)
	if err != nil {
		return nil, fmt.Errorf("fetching users : %w", err)
	}

	var result []*user.User

	for cursor.Next(ctx) {
		var u user.Model
		if cursor.Err() != nil {
			return nil, fmt.Errorf("fetching users : %w", err)
		}

		if err = cursor.Decode(&u); err != nil {
			return nil, fmt.Errorf("fetching users : %w", err)
		}

		result = append(result, u.User())
	}

	return result, nil
}

func (r *UserRepository) Delete(ctx context.Context, id bson.D) error {
	//TODO: WRITE SOME LOGIC WITH TRX ROLLBACK

	_, err := r.db.DeleteOne(ctx, id)

	if err != nil {
		return fmt.Errorf("deleting user : %w", err)
	}

	return nil
}

func (r *UserRepository) Create(ctx context.Context, u *user.Model) (string, error) {
	u.ID = primitive.NewObjectID()
	res, err := r.db.InsertOne(ctx, u)
	if err != nil {
		return "", fmt.Errorf("inserting user : %w", err)
	}
	id := res.InsertedID.(primitive.ObjectID)

	return id.String(), nil
}

func (r *UserRepository) Update(ctx context.Context, filter bson.D, u bson.M) error {
	_, err := r.db.UpdateOne(ctx, filter, u)
	if err != nil {
		return fmt.Errorf("updating user : %w", err)
	}

	return nil
}

func wrapModel(model *user.Model) bson.M {
	res := bson.M{}

	if model.Role != "" {
		res["role"] = model.Role
	}

	if model.Profile != nil {
		p := model.Profile
		profile := bson.M{}
		if p.Email != nil {
			profile["email"] = p.Email
		}
		if p.Username != nil {
			profile["username"] = p.Username
		}
		if p.Firstname != nil {
			profile["firstname"] = p.Firstname
		}
		if p.Lastname != nil {
			profile["lastname"] = p.Lastname
		}
		if p.BirthDate != nil {
			profile["birthdate"] = p.BirthDate
		}
		if p.Password != nil {
			if !(len(*p.Password) < 8 || len(*p.Password) > 16) {
				pass, _ := bcrypt.GenerateFromPassword([]byte(*p.Password), 10)
				profile["password"] = pass
			}
		}
		res["profile"] = profile
	}

	if model.Info != nil {
		i := model.Info
		info := bson.M{}
		if i.CompanyID != uuid.Nil {
			info["company_id"] = i.CompanyID
		}

		if i.DepartmentID != uuid.Nil {
			info["department_id"] = i.DepartmentID
		}

		if i.ProjectID != uuid.Nil {
			info["project_id"] = i.ProjectID
		}

		if i.TeamID != uuid.Nil {
			info["team_id"] = i.TeamID
		}

		if i.PositionID != uuid.Nil {
			info["position_id"] = i.PositionID
		}

		if i.GradeID != uuid.Nil {
			info["grade_id"] = i.GradeID
		}

		if i.Salary != nil {
			info["salary"] = i.Salary
		}

		if i.ActiveVacationDays != nil {
			info["active_vacation_days"] = i.ActiveVacationDays
		}

		if i.IsActive != nil {
			info["is_active"] = i.IsActive
		}

		if i.Skills != nil {
			info["skills"] = i.Skills
		}

		if i.Responsibilities != nil {
			info["responsibilities"] = i.Responsibilities
		}
		res["info"] = info
	}

	return bson.M{"$set": res}
}
