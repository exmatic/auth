package middleware

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	"github.com/pkg/errors"

	"gitlab.com/exmatic/auth/internal/core/role"
	"gitlab.com/exmatic/auth/internal/core/user"
	"gitlab.com/exmatic/auth/pkg/web"
)

func Authorize(userRole ...role.Role) web.Middleware {
	m := func(handler web.Handler) web.Handler {
		h := func(ctx context.Context, w http.ResponseWriter, r *http.Request) error {

			// If the context is missing this value return failure.
			usr := ctx.Value("user").(*user.User)
			if usr == nil {
				return NewRequestError(fmt.Errorf("you are not authorized for the action, no user data found"), http.StatusForbidden)
			}

			if !isMatched(usr.Role, userRole...) {
				return NewRequestError(
					fmt.Errorf("you are not authorized for that action, claims[%v] roles[%v]", usr.Role, userRole),
					http.StatusForbidden,
				)
			}

			return handler(ctx, w, r)
		}

		return h
	}

	return m
}

func isMatched(target role.Role, roles ...role.Role) bool {
	for _, r := range roles {
		if target == r {
			return true
		}
	}

	return false
}

func Authenticate(a user.Auth) web.Middleware {
	m := func(handler web.Handler) web.Handler {
		h := func(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
			// Expecting: bearer <token>
			accessBearer := r.Header.Get("authorization")
			// Parse the authorization header.

			if accessBearer == "" {
				return NewRequestError(errors.New("no authorization header"), http.StatusUnauthorized)
			}

			accessParts := strings.Split(accessBearer, " ")
			if len(accessParts) != 2 || strings.ToLower(accessParts[0]) != "bearer" {
				err := errors.New("expected authorization header format: bearer <token>")
				return NewRequestError(err, http.StatusUnauthorized)
			}

			accessToken := accessParts[1]

			// Validate the token is signed by us.
			usr, err := a.Authenticate(ctx, accessToken)

			if err != nil || usr == nil {
				return NewRequestError(err, http.StatusUnauthorized)
			}

			// Add claims to the context, so they can be retrieved later.
			ctx = context.WithValue(ctx, "user", usr)

			// Call the next handler.
			return handler(ctx, w, r)
		}

		return h
	}

	return m
}
