package user

import (
	"context"
)

type Service interface {
	Create(ctx context.Context, p *Profile) error
	FetchOneByID(ctx context.Context, id string) (*User, error)
	Fetch(ctx context.Context, cond *Cond) ([]*User, error)
	DeleteInfo(ctx context.Context, userID string, companyID string) error
	UpdateProfile(ctx context.Context, p *ProfileModel, userID string) error
	UpdateInfoInit(ctx context.Context, userID string, info *UpdateUserInfo) error
	UpdateInfo(ctx context.Context, userID string, companyID string, info *UpdateUserInfo) error
}

type Cond struct {
	IDs              []string
	Email            []string
	Companies        []string
	Departments      []string
	Projects         []string
	Teams            []string
	Positions        []string
	Grades           []string
	Skills           []string
	Responsibilities []string
}

type Auth interface {
	Login(ctx context.Context, p *Profile) (string, error)
	Authenticate(ctx context.Context, tokens string) (*User, error)
}
