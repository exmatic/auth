package user

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson"
)

type TokenRepository interface {
	Create(ctx context.Context, userID string, tokens []string, exp time.Duration) error
	Fetch(ctx context.Context, key string) ([]string, error)
}

type Repository interface {
	FetchOne(ctx context.Context, filters bson.D) (*User, error)
	Fetch(ctx context.Context, filters bson.D) ([]*User, error)
	Create(ctx context.Context, user *Model) (string, error)
	Update(ctx context.Context, filter bson.D, u bson.M) error
	Delete(ctx context.Context, id bson.D) error
}
