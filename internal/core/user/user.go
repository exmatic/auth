package user

import (
	"time"

	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson/primitive"

	"gitlab.com/exmatic/auth/internal/core/role"
)

type User struct {
	ID        string    `json:"id"`
	Profile   Profile   `json:"profile"`
	Info      *Info     `json:"info"`
	Role      role.Role `json:"role"`
	CreatedAt time.Time `json:"createdAt"`
	UpdatedAt time.Time `json:"updatedAt"`
}

type Profile struct {
	Email     string    `json:"email" bson:"email"`
	Username  string    `json:"username" bson:"username"`
	Firstname string    `json:"firstname" bson:"firstname"`
	Lastname  string    `json:"lastname" bson:"lastname"`
	BirthDate time.Time `json:"birthDate" bson:"birthDate"`
	Password  string    `json:"password" bson:"password"`
}

type Info struct {
	CompanyID          string    `json:"company_id"`
	DepartmentID       string    `json:"department_id"`
	ProjectID          string    `json:"project_id"`
	TeamID             string    `json:"team_id"`
	PositionID         string    `json:"position_id"`
	GradeID            string    `json:"grade_id"`
	Salary             int       `json:"salary"`
	ActiveVacationDays int       `json:"active_vacation_days"`
	IsActive           int       `json:"is_active"`
	Skills             []string  `json:"skills"`
	Responsibilities   []string  `json:"responsibilities"`
	WorkingSince       time.Time `json:"date_working_since"`
}

type Model struct {
	ID        primitive.ObjectID `bson:"_id"`
	Profile   *ProfileModel      `bson:"profile"`
	Info      *InfoModel         `bson:"info"`
	Role      role.Role          `bson:"role"`
	CreatedAt *time.Time         `bson:"createdAt"`
	UpdatedAt *time.Time         `bson:"updatedAt"`
}

type ProfileModel struct {
	Email     *string    `bson:"email"`
	Username  *string    `bson:"username"`
	Firstname *string    `bson:"firstname"`
	Lastname  *string    `bson:"lastname"`
	BirthDate *time.Time `bson:"birthDate"`
	Password  *string    `bson:"password"`
}

type InfoModel struct {
	CompanyID          uuid.UUID  `bson:"company_id"`
	DepartmentID       uuid.UUID  `bson:"department_id"`
	ProjectID          uuid.UUID  `bson:"project_id"`
	TeamID             uuid.UUID  `bson:"team_id"`
	PositionID         uuid.UUID  `bson:"position_id"`
	GradeID            uuid.UUID  `bson:"grade_id"`
	Salary             *int       `bson:"salary"`
	ActiveVacationDays *int       `bson:"active_vacation_days"`
	IsActive           *int       `bson:"is_active"`
	Skills             []string   `bson:"skills"`
	Responsibilities   []string   `bson:"responsibilities"`
	WorkingSince       *time.Time `bson:"date_working_since"`
}

type UpdateUserInfo struct {
	CompanyID          *string    `json:"company_id,omitempty"`
	DepartmentID       *string    `json:"department_id,omitempty"`
	ProjectID          *string    `json:"project_id,omitempty"`
	TeamID             *string    `json:"team_id,omitempty"`
	PositionID         *string    `json:"position_id,omitempty"`
	GradeID            *string    `json:"grade_id,omitempty"`
	Salary             *int       `json:"salary,omitempty"`
	ActiveVacationDays *int       `json:"active_vacation_days,omitempty"`
	IsActive           *int       `json:"is_active,omitempty"`
	Skills             []string   `json:"skills,omitempty"`
	Responsibilities   []string   `json:"responsibilities,omitempty"`
	WorkingSince       *time.Time `json:"date_working_since,omitempty"`
	Role               *role.Role `json:"role,omitempty"`
}
