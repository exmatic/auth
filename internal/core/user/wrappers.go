package user

import (
	"fmt"
	"net/mail"
	"time"

	"github.com/google/uuid"
	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/bson/primitive"

	"gitlab.com/exmatic/auth/internal/core/role"
)

func (u *User) Model() (*Model, error) {
	var uid primitive.ObjectID
	if u.ID != "" {
		var err error
		uid, err = primitive.ObjectIDFromHex(u.ID)
		if err != nil {
			return nil, fmt.Errorf("converting user to model : %w", err)
		}
	}

	model := &Model{
		ID: uid,
		Profile: &ProfileModel{
			Email:     &u.Profile.Email,
			Username:  &u.Profile.Username,
			Firstname: &u.Profile.Firstname,
			Lastname:  &u.Profile.Lastname,
			BirthDate: &u.Profile.BirthDate,
			Password:  &u.Profile.Password,
		},
		Role:      u.Role,
		CreatedAt: &u.CreatedAt,
		UpdatedAt: &u.UpdatedAt,
	}

	if u.Info != nil {
		companyID, err := uuid.Parse(u.Info.CompanyID)

		if err != nil {
			return nil, fmt.Errorf("converting user to model, companyID : %w", err)
		}

		departmentID, err := uuid.Parse(u.Info.DepartmentID)
		if err != nil {
			return nil, fmt.Errorf("converting user to model, departmentID : %w", err)
		}

		projectID, err := uuid.Parse(u.Info.ProjectID)
		if err != nil {
			return nil, fmt.Errorf("converting user to model, projectID : %w", err)
		}

		teamID, err := uuid.Parse(u.Info.TeamID)
		if err != nil {
			return nil, fmt.Errorf("converting user to model, teamID : %w", err)
		}

		positionID, err := uuid.Parse(u.Info.PositionID)
		if err != nil {
			return nil, fmt.Errorf("converting user to model, positionID : %w", err)
		}

		gradeID, err := uuid.Parse(u.Info.GradeID)
		if err != nil {
			return nil, fmt.Errorf("converting user to model, gradeID : %w", err)
		}

		model.Info = &InfoModel{
			CompanyID:          companyID,
			DepartmentID:       departmentID,
			ProjectID:          projectID,
			TeamID:             teamID,
			PositionID:         positionID,
			GradeID:            gradeID,
			Salary:             &u.Info.Salary,
			ActiveVacationDays: &u.Info.ActiveVacationDays,
			IsActive:           &u.Info.IsActive,
			Skills:             u.Info.Skills,
			Responsibilities:   u.Info.Responsibilities,
			WorkingSince:       &u.Info.WorkingSince,
		}
	}

	return model, nil
}

func (m *Model) User() *User {
	var profile Profile
	if m.Profile != nil {
		profile = Profile{
			Email:     wrapString(m.Profile.Email),
			Username:  wrapString(m.Profile.Username),
			Firstname: wrapString(m.Profile.Firstname),
			Lastname:  wrapString(m.Profile.Lastname),
			BirthDate: wrapDate(m.Profile.BirthDate),
			Password:  wrapString(m.Profile.Password),
		}
	}

	var info *Info

	if m.Info != nil {
		info = &Info{
			CompanyID:          m.Info.CompanyID.String(),
			DepartmentID:       m.Info.DepartmentID.String(),
			ProjectID:          m.Info.ProjectID.String(),
			TeamID:             m.Info.TeamID.String(),
			PositionID:         m.Info.PositionID.String(),
			GradeID:            m.Info.GradeID.String(),
			Salary:             wrapInt(m.Info.Salary),
			ActiveVacationDays: wrapInt(m.Info.ActiveVacationDays),
			IsActive:           wrapInt(m.Info.IsActive),
			Skills:             m.Info.Skills,
			Responsibilities:   m.Info.Responsibilities,
			WorkingSince:       wrapDate(m.Info.WorkingSince),
		}
	}
	r := role.User
	if m.Role != "" {
		r = m.Role
	}

	res := &User{
		ID:        m.ID.Hex(),
		Profile:   profile,
		Info:      info,
		Role:      r,
		CreatedAt: wrapDate(m.CreatedAt),
		UpdatedAt: wrapDate(m.UpdatedAt),
	}

	return res
}

func (u *User) UpdateInfo(i *Info) {
	if i == nil {
		return
	}

	if u.Info == nil {
		u.Info = &Info{}
	}

	if i.CompanyID != "" {
		u.Info.CompanyID = i.CompanyID
	} else {
		i.CompanyID = uuid.Nil.String()
	}

	if i.DepartmentID != "" {
		u.Info.DepartmentID = i.DepartmentID
	} else {
		u.Info.DepartmentID = uuid.Nil.String()
	}

	if i.ProjectID != "" {
		u.Info.ProjectID = i.ProjectID
	} else {
		u.Info.ProjectID = uuid.Nil.String()
	}

	if i.TeamID != "" {
		u.Info.TeamID = i.TeamID
	} else {
		u.Info.TeamID = uuid.Nil.String()
	}

	if i.PositionID != "" {
		u.Info.PositionID = i.PositionID
	} else {
		u.Info.PositionID = uuid.Nil.String()
	}

	if i.GradeID != "" {
		u.Info.GradeID = i.GradeID
	} else {
		u.Info.GradeID = uuid.Nil.String()
	}

	if i.Salary != 0 {
		u.Info.Salary = i.Salary
	}

	if i.ActiveVacationDays != 0 {
		u.Info.ActiveVacationDays = i.ActiveVacationDays
	}

	if i.IsActive != 0 {
		u.Info.IsActive = i.IsActive
	}

	if i.Skills != nil {
		u.Info.Skills = i.Skills
	}

	if i.Responsibilities != nil {
		u.Info.Responsibilities = i.Responsibilities
	}
}

func wrapString(val *string) string {
	var res string
	if val != nil {
		res = *val
	}

	return res
}

func wrapDate(val *time.Time) time.Time {
	var res time.Time
	if val != nil {
		res = *val
	}

	return res
}

func wrapInt(val *int) int {
	var res int
	if val != nil {
		res = *val
	}

	return res
}

func (u *Profile) Validate() error {

	if len(u.Password) < 8 {
		return errors.New("length of the password cannot be less than 8")
	}

	if u.Lastname == "" {
		return errors.New("lastname cannot be empty")
	}

	if u.Username == "" {
		return errors.New("username cannot be empty")
	}

	if u.Firstname == "" {
		return errors.New("firstname cannot be empty")
	}

	if u.BirthDate == (time.Time{}) {
		return errors.New("birthdate cannot be empty")
	}

	if _, err := mail.ParseAddress(u.Email); err != nil {
		return errors.New("invalid email")
	}

	return nil

}
