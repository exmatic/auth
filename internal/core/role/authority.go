package role

type Role string

const (
	User     Role = "ROLE_USER"
	Employee Role = "ROLE_EMPLOYEE"
	HR       Role = "ROLE_HR"
	Admin    Role = "ROLE_ADMIN"
)

func ValidateRole(input Role, role Role) bool {
	switch input {
	case "ROLE_USER":
		input = User
	case "ROLE_EMPLOYEE":
		input = Employee
	case "ROLE_HR":
		input = HR
	case "ROLE_ADMIN":
		input = Admin
	default:
		input = ""
	}

	if input == role {
		return true
	}

	return false
}
