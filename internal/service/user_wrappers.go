package service

import (
	"net/mail"

	"github.com/google/uuid"
	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/exmatic/auth/internal/core/role"
	"gitlab.com/exmatic/auth/internal/core/user"
)

func wrapUserCond(cond *user.Cond) bson.D {
	if cond == nil {
		return nil
	}

	var alter int

	res := bson.A{}

	if cond.IDs != nil {
		arr := bson.A{}
		for _, v := range cond.IDs {
			id, err := primitive.ObjectIDFromHex(v)
			if err != nil {
				continue
			}
			arr = append(arr, bson.D{{"_id", id}})
		}

		res = append(res, bson.D{
			{"$or", arr},
		})
		alter++
	}

	if cond.Email != nil {
		arr := bson.A{}
		for _, email := range cond.Email {
			arr = append(arr, bson.D{{"profile.email", email}})
		}

		res = append(res, bson.D{
			{"$or", arr},
		})
		alter++
	}

	if cond.Companies != nil {
		arr := bson.A{}
		for _, comp := range cond.Companies {
			comp_id, err := uuid.Parse(comp)
			if err != nil {
				continue
			}
			arr = append(arr, bson.D{{"info.company_id", comp_id}})
		}

		res = append(res, bson.D{
			{"$or", arr},
		})
		alter++
	}

	if cond.Departments != nil {
		arr := bson.A{}
		for _, dep := range cond.Departments {
			dep_id, err := uuid.Parse(dep)
			if err != nil {
				continue
			}

			arr = append(arr, bson.D{{"info.department_id", dep_id}})
		}

		res = append(res, bson.D{
			{"$or", arr},
		})
		alter++
	}

	if cond.Projects != nil {
		arr := bson.A{}
		for _, project := range cond.Projects {
			project_id, err := uuid.Parse(project)
			if err != nil {
				continue
			}

			arr = append(arr, bson.D{{"info.project_id", project_id}})
		}

		res = append(res, bson.D{
			{"$or", arr},
		})
		alter++
	}

	if cond.Teams != nil {
		arr := bson.A{}
		for _, team := range cond.Teams {
			team_id, err := uuid.Parse(team)
			if err != nil {
				continue
			}

			arr = append(arr, bson.D{{"info.team_id", team_id}})
		}

		res = append(res, bson.D{
			{"$or", arr},
		})
		alter++
	}

	if cond.Positions != nil {
		arr := bson.A{}
		for _, dep := range cond.Positions {
			pos_id, err := uuid.Parse(dep)
			if err != nil {
				continue
			}

			arr = append(arr, bson.D{{"info.position_id", pos_id}})
		}

		res = append(res, bson.D{
			{"$or", arr},
		})
		alter++
	}

	if cond.Grades != nil {
		arr := bson.A{}
		for _, grade := range cond.Grades {
			grade_id, err := uuid.FromBytes([]byte(grade))
			if err != nil {
				continue
			}

			arr = append(arr, bson.D{{"info.grade_id", grade_id}})
		}

		res = append(res, bson.D{
			{"$or", arr},
		})
		alter++
	}

	result := bson.D{}
	if alter > 0 {
		result = bson.D{{"$and", res}}
	}

	return result
}

func validateAndWrapUserUpdateInfo(info *user.UpdateUserInfo) (bson.M, error) {
	if info == nil {
		return nil, errors.New("nil user info")
	}

	res := bson.M{}

	if info.CompanyID != nil {
		compID, err := uuid.Parse(*info.CompanyID)
		if err != nil {
			return nil, err
		}
		res["info.company_id"] = compID
	}

	if info.DepartmentID != nil {
		departmentID, err := uuid.Parse(*info.DepartmentID)
		if err != nil {
			return nil, err
		}
		res["info.department_id"] = departmentID
	}

	if info.ProjectID != nil {
		projectID, err := uuid.Parse(*info.ProjectID)
		if err != nil {
			return nil, err
		}
		res["info.project_id"] = projectID
	}

	if info.TeamID != nil {
		teamID, err := uuid.Parse(*info.TeamID)
		if err != nil {
			return nil, err
		}
		res["info.team_id"] = teamID
	}

	if info.PositionID != nil {
		positionID, err := uuid.Parse(*info.PositionID)
		if err != nil {
			return nil, err
		}
		res["info.position_id"] = positionID
	}

	if info.GradeID != nil {
		gradeID, err := uuid.Parse(*info.GradeID)
		if err != nil {
			return nil, err
		}
		res["info.grade_id"] = gradeID
	}

	if info.Salary != nil {
		res["info.salary"] = *info.Salary
	}

	if info.ActiveVacationDays != nil {
		res["info.active_vacation_days"] = *info.ActiveVacationDays
	}
	if info.IsActive != nil {
		res["info.is_active"] = *info.IsActive
	}
	if info.Skills != nil {
		res["info.skills"] = info.Skills
	}
	if info.Responsibilities != nil {
		res["info.responsibilities"] = info.Responsibilities
	}
	if info.WorkingSince != nil {
		res["info.date_working_since"] = info.WorkingSince
	}

	if info.Role != nil {
		var newRole role.Role
		switch *info.Role {
		case role.User, role.Employee, role.Admin, role.HR:
			newRole = *info.Role
		default:
			return nil, errors.New("invalid role")
		}

		res["role"] = newRole
	}

	return bson.M{"$set": res}, nil
}

func validateAndWrapUserProfile(p *user.ProfileModel) (bson.M, error) {
	if p == nil {
		return nil, errors.New("nil user info")
	}

	res := bson.M{}

	if p.Email != nil {
		if _, err := mail.ParseAddress(*p.Email); err != nil {
			return nil, err
		}
		res["profile.email"] = *p.Email
	}

	if p.Username != nil {
		res["profile.username"] = *p.Username
	}
	if p.Firstname != nil {
		res["profile.firstname"] = *p.Firstname
	}
	if p.Lastname != nil {
		res["profile.lastname"] = p.Lastname
	}
	if p.BirthDate != nil {
		res["profile.birthdate"] = *p.BirthDate
	}
	if p.Password != nil {
		if len(*p.Password) < 8 {
			return nil, errors.New("password length must be >=8")
		}

		cp, err := bcrypt.GenerateFromPassword([]byte(*p.Password), 10)
		if err != nil {
			return nil, errors.Wrap(errors.New("invalid password"), err.Error())
		}

		res["profile.password"] = string(cp)
	}

	return bson.M{"$set": res}, nil
}
