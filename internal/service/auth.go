package service

import (
	"context"

	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/exmatic/auth/internal/core/user"
	"gitlab.com/exmatic/auth/pkg/jwt"
)

var (
	ErrNilProfile   = errors.New("nil profile")
	ErrTokenExpired = errors.New("token is expired")
	ErrInvalidToken = errors.New("token is invalid")
)

type Auth struct {
	userRepository user.Repository
	jwt            *jwt.JWT
}

func NewAuth(jwt *jwt.JWT, userRepo user.Repository) user.Auth {
	return &Auth{
		userRepository: userRepo,
		jwt:            jwt,
	}
}

func (a *Auth) Login(ctx context.Context, p *user.Profile) (string, error) {
	if p == nil {
		return "", ErrNilProfile
	}

	usr, err := a.userRepository.FetchOne(ctx, bson.D{{
		"$or", bson.A{
			bson.D{{"profile.username", p.Username}},
			bson.D{{"profile.email", p.Email}},
		},
	}})

	if err != nil {
		return "", errors.New("wrong username or password")
	}

	if err = bcrypt.CompareHashAndPassword([]byte(usr.Profile.Password), []byte(p.Password)); err != nil {
		return "", errors.New("wrong username or password")
	}

	c := &jwt.Claims{
		UserID: usr.ID,
		Role:   string(usr.Role),
	}

	token, err := a.jwt.Generate(c)
	if err != nil {
		return "", err
	}

	return token, nil
}

func (a *Auth) Authenticate(ctx context.Context, token string) (*user.User, error) {
	c, err := a.jwt.VerifyAccessTokenWithClaims(token)
	if err != nil {
		return nil, ErrInvalidToken
	}

	if !a.jwt.VerifyExp(c.Exp) {
		return nil, ErrTokenExpired
	}

	id, err := primitive.ObjectIDFromHex(c.UserID)
	if err != nil {
		return nil, errors.New("invalid user id")
	}

	usr, err := a.userRepository.FetchOne(ctx, bson.D{{"_id", id}})
	if err != nil {
		return nil, err
	}

	return usr, nil
}
