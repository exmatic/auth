package service

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/exmatic/auth/internal/core/role"
	"gitlab.com/exmatic/auth/internal/core/user"
)

var (
	ErrInvalidUserID   = errors.New("invalid userID")
	ErrNilUser         = errors.New("nil user")
	ErrNoPermission    = errors.New("no permission")
	ErrNoUserInfo      = errors.New("no user info")
	ErrUserNotFound    = errors.New("user not found")
	ErrInvalidUserInfo = errors.New("invalid user info")
	ErrInvalidCompany  = errors.New("invalid company")
)

type UserService struct {
	repo user.Repository
}

func NewUserService(repo user.Repository) user.Service {
	return &UserService{
		repo: repo,
	}
}

func (s *UserService) Create(ctx context.Context, p *user.Profile) error {
	if p == nil {
		return errors.New("nil profile error")
	}

	//Validating user for fields
	if err := p.Validate(); err != nil {
		return err
	}

	password, err := bcrypt.GenerateFromPassword([]byte(p.Password), 10)
	if err != nil {
		return fmt.Errorf("creating user : %w", err)
	}

	p.Password = string(password)

	newUsr := user.User{
		Profile:   *p,
		Info:      nil,
		Role:      role.User,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}

	model, err := newUsr.Model()
	if err != nil {
		return fmt.Errorf("wrong data unable to decode : %w", err)
	}

	if _, err = s.repo.Create(ctx, model); err != nil {
		return fmt.Errorf("something went wrong : %w", err)
	}
	log.Println("after create")

	return nil
}

func (s *UserService) FetchOneByID(ctx context.Context, id string) (*user.User, error) {
	userId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}

	usr, err := s.repo.FetchOne(ctx, bson.D{{"_id", userId}})
	if err != nil {
		return nil, err
	}

	return usr, nil
}

func (s *UserService) Fetch(ctx context.Context, cond *user.Cond) ([]*user.User, error) {
	if cond == nil {
		return nil, nil
	}

	users, err := s.repo.Fetch(ctx, wrapUserCond(cond))
	if err != nil {
		return nil, fmt.Errorf("service : fetching users : %w", err)
	}

	return users, nil
}

func (s *UserService) DeleteInfo(ctx context.Context, userID string, companyID string) error {
	id, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		return fmt.Errorf("deleting user info : %w", err)
	}

	usr, err := s.repo.FetchOne(ctx, bson.D{{"_id", id}})
	if err != nil {
		return fmt.Errorf("deleting user info : %w", err)
	}

	if usr.Info == nil {
		return ErrNoUserInfo
	}

	if usr.Info.CompanyID != companyID {
		return ErrNoPermission
	}

	usr.Info = nil

	model, err := usr.Model()
	if err != nil {
		return fmt.Errorf("deleting user info : %w", err)
	}

	//bson map for deleting user info
	//userRepo.Update receives only bson map data
	u := bson.M{
		"$set": bson.M{
			"info": nil,
			"role": role.User,
		},
	}

	if err = s.repo.Update(ctx, bson.D{{"_id", model.ID}}, u); err != nil {
		return fmt.Errorf("deleting usr info : %w", err)
	}

	return nil
}

func (s *UserService) UpdateInfo(ctx context.Context, userID string, companyID string, info *user.UpdateUserInfo) error {
	if info == nil {
		return ErrNilUser
	}

	id, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		return ErrInvalidUserID
	}

	usr, err := s.repo.FetchOne(ctx, bson.D{{"_id", id}})
	if err != nil || usr == nil {
		return ErrUserNotFound
	}

	if usr.Info == nil {
		if err := s.repo.Update(ctx, bson.D{{"_id", id}}, bson.M{"$set": bson.M{"info": bson.M{}}}); err != nil {
			return errors.Wrap(errors.New("unable to update"), err.Error())
		}
	}

	if usr.Info.CompanyID != companyID {
		return ErrInvalidCompany
	}

	u, err := validateAndWrapUserUpdateInfo(info)
	if err != nil {
		return errors.Wrap(ErrInvalidUserInfo, err.Error())
	}

	if err = s.repo.Update(ctx, bson.D{{"_id", id}}, u); err != nil {
		return errors.Wrap(errors.New("unable to update"), err.Error())
	}

	return nil
}

func (s *UserService) UpdateInfoInit(ctx context.Context, userID string, info *user.UpdateUserInfo) error {
	if info == nil {
		return ErrNilUser
	}

	id, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		return ErrInvalidUserID
	}

	usr, err := s.repo.FetchOne(ctx, bson.D{{"_id", id}})
	if err != nil || usr == nil {
		return ErrUserNotFound
	}

	if usr.Info == nil {
		if err := s.repo.Update(ctx, bson.D{{"_id", id}}, bson.M{"$set": bson.M{"info": bson.M{}}}); err != nil {
			return errors.Wrap(errors.New("unable to update"), err.Error())
		}
	}

	u, err := validateAndWrapUserUpdateInfo(info)
	if err != nil {
		return errors.Wrap(ErrInvalidUserInfo, err.Error())
	}

	if err = s.repo.Update(ctx, bson.D{{"_id", id}}, u); err != nil {
		return errors.Wrap(errors.New("unable to update"), err.Error())
	}

	return nil
}

func (s *UserService) UpdateProfile(ctx context.Context, p *user.ProfileModel, userID string) error {
	if p == nil {
		return ErrNoUserInfo
	}

	id, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		return ErrInvalidUserID
	}

	u, err := validateAndWrapUserProfile(p)
	if err != nil {
		return err
	}

	if err = s.repo.Update(ctx, bson.D{{"_id", id}}, u); err != nil {
		return err
	}

	return nil
}
