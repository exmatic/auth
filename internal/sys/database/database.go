package database

import (
	"context"
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func addIndex(db *mongo.Database, indexKeys interface{}) error {
	coll := db.Collection("users")
	indexName, err := coll.Indexes().CreateOne(context.Background(), mongo.IndexModel{
		Keys:    indexKeys,
		Options: options.Index().SetUnique(true),
	})
	if err != nil {
		return err
	}
	fmt.Println(indexName)
	return nil
}

func OpenMongoConn(dsn string, username string, password string, dbname string) (*mongo.Database, error) {
	ctx := context.Background()
	clientOpts := options.Client().ApplyURI(dsn)
	client, err := mongo.Connect(ctx, clientOpts)
	if err != nil {
		return nil, err
	}

	if err = addIndex(client.Database(dbname), bson.M{"profile.email": 1}); err != nil {
		return nil, err
	}

	if err = addIndex(client.Database(dbname), bson.M{"profile.username": 1}); err != nil {
		return nil, err
	}

	return client.Database(dbname), nil
}
