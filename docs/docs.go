// Package docs GENERATED BY SWAG; DO NOT EDIT
// This file was generated by swaggo/swag
package docs

import "github.com/swaggo/swag"

const docTemplate = `{
    "schemes": {{ marshal .Schemes }},
    "swagger": "2.0",
    "info": {
        "description": "{{escape .Description}}",
        "title": "{{.Title}}",
        "contact": {},
        "version": "{{.Version}}"
    },
    "host": "{{.Host}}",
    "basePath": "{{.BasePath}}",
    "paths": {
        "/user": {
            "get": {
                "security": [
                    {
                        "ApiKeyAuth": []
                    }
                ],
                "description": "returns yourself",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "user"
                ],
                "summary": "GetSelf user",
                "responses": {
                    "200": {
                        "description": "httpStatusOk",
                        "schema": {
                            "type": "integer"
                        }
                    },
                    "500": {
                        "description": "httpStatusInternalServerError",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    },
                    "default": {
                        "description": "",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    }
                }
            },
            "post": {
                "description": "creates user.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "user"
                ],
                "summary": "Create user",
                "parameters": [
                    {
                        "description": "user profile",
                        "name": "input",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/user.Profile"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "httpStatusOk",
                        "schema": {
                            "type": "integer"
                        }
                    },
                    "400": {
                        "description": "httpStatusBadRequest",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    },
                    "500": {
                        "description": "httpStatusInternalServerError",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    },
                    "default": {
                        "description": "",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    }
                }
            }
        },
        "/user/info/init/{id}": {
            "put": {
                "description": "updates user info. Info contains company related info. Used specifically for ORG microservice to set company_id and role without authorization. Normally, to update user use UpdateUserInfo with the right privilleges.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "user"
                ],
                "summary": "UpdateUserInfoInit user",
                "parameters": [
                    {
                        "type": "string",
                        "description": "user ID",
                        "name": "id",
                        "in": "path",
                        "required": true
                    },
                    {
                        "description": "user info",
                        "name": "input",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/user.UpdateUserInfo"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "httpStatusOk",
                        "schema": {
                            "type": "integer"
                        }
                    },
                    "400": {
                        "description": "httpStatusBadRequest",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    },
                    "401": {
                        "description": "httpStatusUnauthorized",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    },
                    "500": {
                        "description": "httpStatusInternalServerError",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    },
                    "default": {
                        "description": "",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    }
                }
            }
        },
        "/user/info/{id}": {
            "put": {
                "security": [
                    {
                        "ApiKeyAuth": []
                    }
                ],
                "description": "updates user info. Info contains company related info. Can only be accessed by roles: Admin and HR.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "user"
                ],
                "summary": "UpdateUserInfo user",
                "parameters": [
                    {
                        "type": "string",
                        "description": "user ID",
                        "name": "id",
                        "in": "path",
                        "required": true
                    },
                    {
                        "description": "user info",
                        "name": "input",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/user.UpdateUserInfo"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "httpStatusOk",
                        "schema": {
                            "type": "integer"
                        }
                    },
                    "400": {
                        "description": "httpStatusBadRequest",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    },
                    "401": {
                        "description": "httpStatusUnauthorized",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    },
                    "403": {
                        "description": "httpStatusForbidden",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    },
                    "500": {
                        "description": "httpStatusInternalServerError",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    },
                    "default": {
                        "description": "",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    }
                }
            },
            "delete": {
                "security": [
                    {
                        "ApiKeyAuth": []
                    }
                ],
                "description": "deletes user info.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "user"
                ],
                "summary": "DeleteUserInfo user",
                "parameters": [
                    {
                        "type": "string",
                        "description": "user ID",
                        "name": "id",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "httpStatusOk",
                        "schema": {
                            "type": "integer"
                        }
                    },
                    "400": {
                        "description": "httpStatusBadRequest",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    },
                    "401": {
                        "description": "httpStatusUnauthorized",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    },
                    "500": {
                        "description": "httpStatusInternalServerError",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    },
                    "default": {
                        "description": "",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    }
                }
            }
        },
        "/user/login": {
            "post": {
                "description": "logins user. Actually only username/email and password needed. Other fields can be skipped.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "user"
                ],
                "summary": "Login user",
                "parameters": [
                    {
                        "description": "profile info",
                        "name": "input",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/user.Profile"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "httpStatusOk",
                        "schema": {
                            "type": "integer"
                        }
                    },
                    "400": {
                        "description": "httpStatusBadRequest",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    },
                    "401": {
                        "description": "httpStatusUnauthorized",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    },
                    "500": {
                        "description": "httpStatusInternalServerError",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    },
                    "default": {
                        "description": "",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    }
                }
            }
        },
        "/user/{id}": {
            "get": {
                "security": [
                    {
                        "ApiKeyAuth": []
                    }
                ],
                "description": "returns a user by its ID",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "user"
                ],
                "summary": "GetOne user",
                "parameters": [
                    {
                        "type": "string",
                        "description": "user ID",
                        "name": "id",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "httpStatusOk",
                        "schema": {
                            "type": "integer"
                        }
                    },
                    "400": {
                        "description": "httpStatusBadRequest",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    },
                    "500": {
                        "description": "httpStatusInternalServerError",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    },
                    "default": {
                        "description": "",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    }
                }
            },
            "put": {
                "security": [
                    {
                        "ApiKeyAuth": []
                    }
                ],
                "description": "updates user profile. Profile contains users private data",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "user"
                ],
                "summary": "UpdateUserProfile user",
                "parameters": [
                    {
                        "type": "string",
                        "description": "user ID",
                        "name": "id",
                        "in": "path",
                        "required": true
                    },
                    {
                        "description": "profile model",
                        "name": "input",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/user.ProfileModel"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "httpStatusOk",
                        "schema": {
                            "type": "integer"
                        }
                    },
                    "400": {
                        "description": "httpStatusBadRequest",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    },
                    "401": {
                        "description": "httpStatusUnauthorized",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    },
                    "500": {
                        "description": "httpStatusInternalServerError",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    },
                    "default": {
                        "description": "",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    }
                }
            }
        },
        "/users": {
            "get": {
                "security": [
                    {
                        "ApiKeyAuth": []
                    }
                ],
                "description": "returns all users by filters.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "user"
                ],
                "summary": "GetAllWithFilters user",
                "parameters": [
                    {
                        "type": "string",
                        "description": "user_id",
                        "name": "id",
                        "in": "query"
                    },
                    {
                        "type": "string",
                        "description": "email",
                        "name": "email",
                        "in": "query"
                    },
                    {
                        "type": "string",
                        "description": "company_id",
                        "name": "company_id",
                        "in": "query"
                    },
                    {
                        "type": "string",
                        "description": "department_id",
                        "name": "department_id",
                        "in": "query"
                    },
                    {
                        "type": "string",
                        "description": "project_id",
                        "name": "project_id",
                        "in": "query"
                    },
                    {
                        "type": "string",
                        "description": "team_id",
                        "name": "team_id",
                        "in": "query"
                    },
                    {
                        "type": "string",
                        "description": "position_id",
                        "name": "position_id",
                        "in": "query"
                    },
                    {
                        "type": "string",
                        "description": "grade_id",
                        "name": "grade_id",
                        "in": "query"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "httpStatusOk",
                        "schema": {
                            "type": "integer"
                        }
                    },
                    "500": {
                        "description": "httpStatusInternalServerError",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    },
                    "default": {
                        "description": "",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    }
                }
            }
        },
        "/verify": {
            "post": {
                "description": "verifies user by token.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "verify"
                ],
                "summary": "Verify user",
                "parameters": [
                    {
                        "description": "jwt token",
                        "name": "input",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/usergrp.Token"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "httpStatusOk",
                        "schema": {
                            "type": "integer"
                        }
                    },
                    "400": {
                        "description": "httpStatusBadRequest",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    },
                    "500": {
                        "description": "httpStatusInternalServerError",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    },
                    "default": {
                        "description": "",
                        "schema": {
                            "$ref": "#/definitions/middleware.ErrorResponse"
                        }
                    }
                }
            }
        }
    },
    "definitions": {
        "middleware.ErrorResponse": {
            "type": "object",
            "properties": {
                "error": {
                    "type": "string"
                },
                "fields": {
                    "type": "object",
                    "additionalProperties": {
                        "type": "string"
                    }
                }
            }
        },
        "user.Profile": {
            "type": "object",
            "properties": {
                "birthDate": {
                    "type": "string"
                },
                "email": {
                    "type": "string"
                },
                "firstname": {
                    "type": "string"
                },
                "lastname": {
                    "type": "string"
                },
                "password": {
                    "type": "string"
                },
                "username": {
                    "type": "string"
                }
            }
        },
        "user.ProfileModel": {
            "type": "object",
            "properties": {
                "birthDate": {
                    "type": "string"
                },
                "email": {
                    "type": "string"
                },
                "firstname": {
                    "type": "string"
                },
                "lastname": {
                    "type": "string"
                },
                "password": {
                    "type": "string"
                },
                "username": {
                    "type": "string"
                }
            }
        },
        "user.UpdateUserInfo": {
            "type": "object",
            "properties": {
                "active_vacation_days": {
                    "type": "integer"
                },
                "company_id": {
                    "type": "string"
                },
                "date_working_since": {
                    "type": "string"
                },
                "department_id": {
                    "type": "string"
                },
                "grade_id": {
                    "type": "string"
                },
                "is_active": {
                    "type": "integer"
                },
                "position_id": {
                    "type": "string"
                },
                "project_id": {
                    "type": "string"
                },
                "responsibilities": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "role": {
                    "type": "string"
                },
                "salary": {
                    "type": "integer"
                },
                "skills": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "team_id": {
                    "type": "string"
                }
            }
        },
        "usergrp.Token": {
            "type": "object",
            "properties": {
                "token": {
                    "type": "string"
                }
            }
        }
    },
    "securityDefinitions": {
        "ApiKeyAuth": {
            "type": "apiKey",
            "name": "Authorization",
            "in": "header"
        }
    }
}`

// SwaggerInfo holds exported Swagger Info so clients can modify it
var SwaggerInfo = &swag.Spec{
	Version:          "1.0",
	Host:             "localhost:8080",
	BasePath:         "/v1/",
	Schemes:          []string{},
	Title:            "exmatic/auth API",
	Description:      "API Server for Auth microservice as a part of exmatic app",
	InfoInstanceName: "swagger",
	SwaggerTemplate:  docTemplate,
}

func init() {
	swag.Register(SwaggerInfo.InstanceName(), SwaggerInfo)
}
