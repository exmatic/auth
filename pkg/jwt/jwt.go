package jwt

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"time"
)

var refreshDuration = time.Hour * 24 * 30
var accessDuration = time.Hour * 24
var globalCounter = 0

type Claims struct {
	UserID string `json:"user_id"`
	Role   string `json:"role"`
	Exp    int64  `json:"exp"`
}

type RefreshPayload struct {
	UserID  string `json:"user_id"`
	Exp     int64  `json:"exp"`
	Counter int64  `json:"counter"`
}

func (c *Claims) Validate() error {
	if c.UserID == "" {
		return errors.New("no user data")
	}
	if c.Role == "" {
		return errors.New("no user data")
	}

	return nil
}

type JWT struct {
	privateKey []byte
}

func NewJWT(key []byte) *JWT {
	return &JWT{
		privateKey: key,
	}
}

// GeneratePair Generates Access and Refresh Tokens pair
// string[0] is access token
// string[1] is refresh token
func (a *JWT) GeneratePair(c *Claims, now *time.Time) ([]string, error) {
	if c == nil {
		return nil, errors.New("payload is empty")
	}

	if err := c.Validate(); err != nil {
		return nil, fmt.Errorf("generating token pair : %w", err)
	}

	enc := base64.RawURLEncoding

	h := struct {
		Alg string `json:"alg"`
		Typ string `json:"typ"`
	}{
		Alg: "HS256",
		Typ: "JWT",
	}

	headerJson, err := json.Marshal(h)
	if err != nil {
		return nil, fmt.Errorf("generating token pair : %w", err)
	}

	c.Exp = now.Add(accessDuration).Unix()

	claimsJson, err := json.Marshal(c)
	if err != nil {
		return nil, fmt.Errorf("generating token pair : %w", err)
	}

	b64header := enc.EncodeToString(headerJson)
	b64atClaims := enc.EncodeToString(claimsJson)

	atHash := hmac.New(sha256.New, a.privateKey)
	atHash.Write([]byte(b64header + "." + b64atClaims))
	atToken := b64header + "." + b64atClaims + "." + enc.EncodeToString(atHash.Sum(nil))
	globalCounter++
	rtClaims := RefreshPayload{
		UserID:  c.UserID,
		Exp:     now.Add(refreshDuration).Unix(),
		Counter: int64(globalCounter),
	}

	rtClaimsJson, err := json.Marshal(rtClaims)
	if err != nil {
		return nil, fmt.Errorf("generating token pair : %w", err)
	}

	b64rtClaims := enc.EncodeToString(rtClaimsJson)

	rtHash := hmac.New(sha256.New, a.privateKey)
	rtHash.Write([]byte(b64header + "." + b64rtClaims))

	rtToken := b64header + "." + b64rtClaims + "." + enc.EncodeToString(rtHash.Sum(nil))

	return []string{atToken, rtToken}, nil
}

func (a *JWT) Generate(c *Claims) (string, error) {
	if c == nil {
		return "", errors.New("payload is empty")
	}

	if err := c.Validate(); err != nil {
		return "", fmt.Errorf("generating token pair : %w", err)
	}

	enc := base64.RawURLEncoding

	h := struct {
		Alg string `json:"alg"`
		Typ string `json:"typ"`
	}{
		Alg: "HS256",
		Typ: "JWT",
	}

	headerJson, err := json.Marshal(h)
	if err != nil {
		return "", fmt.Errorf("generating token pair : %w", err)
	}

	now := time.Now()

	c.Exp = now.Add(accessDuration).Unix()

	claimsJson, err := json.Marshal(c)
	if err != nil {
		return "", fmt.Errorf("generating token pair : %w", err)
	}

	b64header := enc.EncodeToString(headerJson)
	b64atClaims := enc.EncodeToString(claimsJson)

	atHash := hmac.New(sha256.New, a.privateKey)
	atHash.Write([]byte(b64header + "." + b64atClaims))
	atToken := b64header + "." + b64atClaims + "." + enc.EncodeToString(atHash.Sum(nil))

	return atToken, nil
}

//Verify verifies whether token valid or not
func (a *JWT) Verify(t string) bool {
	if t == "" {
		return false
	}
	token := strings.Split(t, ".")
	if len(token) != 3 {
		return false
	}

	header := token[0]
	payload := token[1]
	signature := token[2]

	hash := hmac.New(sha256.New, a.privateKey)
	hash.Write([]byte(header + "." + payload))
	sign := hash.Sum(nil)

	if signature != base64.RawURLEncoding.EncodeToString(sign) {
		return false
	}

	return true
}

//VerifyAccessTokenWithClaims returns error if token is invalid
func (a *JWT) VerifyAccessTokenWithClaims(t string) (*Claims, error) {
	if !a.Verify(t) {
		return nil, errors.New("cannot verify access token")
	}

	token := strings.Split(t, ".")
	payload := token[1]

	payloadBytes, err := base64.RawURLEncoding.DecodeString(payload)
	if err != nil {
		return nil, fmt.Errorf("verifying token : %w", err)
	}

	var claims Claims
	if err = json.Unmarshal(payloadBytes, &claims); err != nil {
		return nil, fmt.Errorf("verifiying token : %w", err)
	}

	return &claims, nil
}

func (a *JWT) VerifyExp(exp int64) bool {
	expTime := time.Unix(exp, 0)
	if time.Now().After(expTime) {
		return false
	}

	return true
}

func (a *JWT) VerifyRefreshTokenWithClaims(t string) (*RefreshPayload, error) {
	if !a.Verify(t) {
		return nil, errors.New("verifying refresh token, invalid token")
	}

	token := strings.Split(t, ".")

	payloadString := token[1]
	payloadBytes, err := base64.RawURLEncoding.DecodeString(payloadString)
	if err != nil {
		return nil, fmt.Errorf("verifying token : %w", err)
	}

	var payload RefreshPayload

	if err = json.Unmarshal(payloadBytes, &payload); err != nil {
		return nil, fmt.Errorf("verifying refresh token : %w", err)
	}

	if !a.VerifyExp(payload.Exp) {
		return nil, errors.New("verifying refresh token, token is expired")
	}

	return &payload, nil
}
