package jwt

import (
	"testing"
	"time"
)

func TestAuth_GeneratePair(t *testing.T) {
	auth := NewJWT([]byte("vanaltercolt"))

	now := time.Now()
	tokenPairs, err := auth.GeneratePair(&Claims{
		UserID: "aybarreluserid",
		Role:   "ROLE_USER",
		Exp:    time.Now().Add(time.Minute * 15).Unix(),
	}, &now)
	if err != nil {
		t.Fatalf("error : %v", err)
	}

	t.Log(tokenPairs[0])
	t.Log('\n')
	t.Log(tokenPairs[1])
}

func TestAuth_Verify(t *testing.T) {
	at := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiYXliYXJyZWx1c2VyaWQiLCJjb21wYW55X2lkIjoiY29tcGFueWlkIiwicm9sZSI6IlJPTEVfVVNFUiIsImV4cCI6MTY1MjkxNDMxOH0.kvK6wA5hrkj42nx9Cz4c0NMGpLonm2NTuQkybfAN55Q"

	rt := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiYXliYXJyZWx1c2VyaWQiLCJleHAiOjE2NTU1MDU0MTh9.KATW2O5jKgyHeU6jeO3Cjm65zkNf52Vkkz6YLwdWBEA"
	auth := NewJWT([]byte("vanaltercolt"))
	atRes := auth.Verify(at)
	rtRes := auth.Verify(rt)
	if !atRes || !rtRes {
		t.Fatal("error : verification does not work")
	}
	t.Log("Access Token Verification :", atRes)
	t.Log("Refresh Token Verification :", rtRes)

	rt = rt + "a"
	at = at + "a"

	atRes = auth.Verify(at)
	rtRes = auth.Verify(rt)

	if atRes || rtRes {
		t.Fatal("error : verification does not work")
	}

	t.Log("Invalid Access Token Verification :", atRes)
	t.Log("Invalid Refresh Token Verification :", rtRes)
}

func TestAuth_VerifyAccessTokenWithClaims(t *testing.T) {
	at := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiYXliYXJyZWx1c2VyaWQiLCJjb21wYW55X2lkIjoiY29tcGFueWlkIiwicm9sZSI6IlJPTEVfVVNFUiIsImV4cCI6MTY1MjkxNDMxOH0.kvK6wA5hrkj42nx9Cz4c0NMGpLonm2NTuQkybfAN55Q"

	auth := NewJWT([]byte("vanaltercolt"))
	res, err := auth.VerifyAccessTokenWithClaims(at)
	if err != nil {
		t.Fatalf("err : %v", err)
	}

	t.Log(*res)
}

func TestAuth_VerifyRefreshTokenWithClaims(t *testing.T) {
	rt := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiYXliYXJyZWx1c2VyaWQiLCJleHAiOjE2NTU1MDU0MTh9.KATW2O5jKgyHeU6jeO3Cjm65zkNf52Vkkz6YLwdWBEA"

	auth := NewJWT([]byte("vanaltercolt"))
	res, err := auth.VerifyRefreshTokenWithClaims(rt)
	if err != nil {
		t.Fatalf("err : %v", err)
	}

	t.Log(*res)
}
