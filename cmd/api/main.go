package main

import (
	"context"
	"expvar"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"go.uber.org/automaxprocs/maxprocs"
	"go.uber.org/zap"

	"gitlab.com/exmatic/auth/cmd/api/handlers"
	"gitlab.com/exmatic/auth/internal/repository/mongo"
	"gitlab.com/exmatic/auth/internal/service"
	"gitlab.com/exmatic/auth/internal/sys/database"
	"gitlab.com/exmatic/auth/pkg/jwt"
	"gitlab.com/exmatic/auth/pkg/logger"
)

// @title exmatic/auth API
// @version 1.0
// @description API Server for Auth microservice as a part of exmatic app

// @host localhost:8080
// @BasePath /v1/

// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization

// Swagger available on localhost:8080/v1/swagger/index.html

// build is the git version of this program. It is set using build flags in the makefile.
var build = "develop"

func main() {

	// Construct the application logger.
	log, err := logger.New("EXMATIC-AUTH")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer log.Sync()

	// Perform the startup and shutdown sequence.
	if err := run(log); err != nil {
		log.Errorw("startup", "ERROR", err)
		log.Sync()
		os.Exit(1)
	}
}

func run(log *zap.SugaredLogger) error {

	// =========================================================================
	// GOMAXPROCS

	// Want to see what maxprocs reports.
	opt := maxprocs.Logger(log.Infof)

	// Set the correct number of threads for the service
	// based on what is available either by the machine or quotas.
	if _, err := maxprocs.Set(opt); err != nil {
		return fmt.Errorf("maxprocs: %w", err)
	}
	log.Infow("startup", "GOMAXPROCS", runtime.GOMAXPROCS(0))

	// =========================================================================
	// App Starting

	log.Infow("starting service", "version", build)
	defer log.Infow("shutdown complete")

	expvar.NewString("build").Set(build)
	// =========================================================================
	// App Starting

	log.Infow("starting service", "version", build)
	defer log.Infow("shutdown complete")

	// =========================================================================
	// Initialize authentication support

	// =========================================================================
	// Database Supports

	mongodb, err := database.OpenMongoConn("mongodb://localhost:27017", "", "", "exmatic-auth")
	if err != nil {
		return fmt.Errorf("connecting to mongo: %w", err)
	}
	defer func() {
		log.Infow("shutdown", "status", "stopping database support", "host", "mongodb://localhost:27017")
		mongodb.Client().Disconnect(context.Background())
	}()

	// =========================================================================
	// Start API Service

	log.Infow("startup", "status", "initializing V1 API support")

	// Make a channel to listen for an interrupt or terminate signal from the OS.
	// Use a buffered channel because the signal package requires it.
	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, syscall.SIGINT, syscall.SIGTERM)

	jwt := jwt.NewJWT([]byte("sukablyatkakzheyazayebalsyazachtoprosto"))
	userRepo := mongo.NewUserRepository(mongodb)
	auth := service.NewAuth(jwt, userRepo)

	// Construct the mux for the API calls.
	apiMux := handlers.API(shutdown, log, mongodb, auth)

	// Construct a server to service the requests against the mux.
	api := http.Server{
		Addr:     "localhost:8080",
		Handler:  apiMux,
		ErrorLog: zap.NewStdLog(log.Desugar()),
	}

	// Make a channel to listen for errors coming from the listener. Use a
	// buffered channel so the goroutine can exit if we don't collect this error.
	serverErrors := make(chan error, 1)

	// Start the service listening for api requests.
	go func() {
		log.Infow("startup", "status", "api router started", "host", api.Addr)
		serverErrors <- api.ListenAndServe()
	}()

	// =========================================================================
	// Shutdown

	// Blocking main and waiting for shutdown.
	select {
	case err := <-serverErrors:
		return fmt.Errorf("server error: %w", err)

	case sig := <-shutdown:
		log.Infow("shutdown", "status", "shutdown started", "signal", sig)
		defer log.Infow("shutdown", "status", "shutdown complete", "signal", sig)

		// Give outstanding requests a deadline for completion.
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()

		// Asking listener to shut down and shed load.
		if err := api.Shutdown(ctx); err != nil {
			api.Close()
			return fmt.Errorf("could not stop server gracefully: %w", err)
		}
	}

	return nil
}
