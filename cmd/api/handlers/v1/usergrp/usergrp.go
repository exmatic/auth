package usergrp

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/exmatic/auth/internal/core/user"
	"gitlab.com/exmatic/auth/internal/middleware"
	"gitlab.com/exmatic/auth/internal/service"
	"gitlab.com/exmatic/auth/pkg/web"
)

type Handler struct {
	userService user.Service
	auth        user.Auth
}

func NewHandler(auth user.Auth, userService user.Service) *Handler {
	return &Handler{
		userService: userService,
		auth:        auth,
	}
}

// @Summary GetOne user
// @Security ApiKeyAuth
// @Tags user
// @Description returns a user by its ID
// @Accept  json
// @Produce json
// @Param id path string true "user ID"
// @Success 200 {integer} integer "httpStatusOk"
// @Failure 400 {object} middleware.ErrorResponse "httpStatusBadRequest"
// @Failure 500 {object} middleware.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} middleware.ErrorResponse
// @Router /user/{id} [get]
func (h *Handler) GetOne(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	userID := web.Param(r, "id")
	if userID == "" {
		return middleware.NewRequestError(errors.New("no user id"), http.StatusBadRequest)
	}

	usr, err := h.userService.FetchOneByID(ctx, userID)
	if err != nil {
		return middleware.NewRequestError(err, http.StatusBadRequest)
	}

	return web.Respond(ctx, w, usr, http.StatusOK)
}

// @Summary Login user
// @Tags user
// @Description logins user. Actually only username/email and password needed. Other fields can be skipped.
// @Accept  json
// @Produce json
// @Param input body user.Profile true "profile info"
// @Success 200 {integer} integer "httpStatusOk"
// @Failure 400 {object} middleware.ErrorResponse "httpStatusBadRequest"
// @Failure 401 {object} middleware.ErrorResponse "httpStatusUnauthorized"
// @Failure 500 {object} middleware.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} middleware.ErrorResponse
// @Router /user/login [post]
func (h *Handler) Login(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	_, err := web.GetValues(ctx)
	if err != nil {
		return web.NewShutdownError("web values are missing from context")
	}

	var usr user.Profile
	if err = web.Decode(r, &usr); err != nil {
		return middleware.NewRequestError(err, http.StatusBadRequest)
	}

	token, err := h.auth.Login(ctx, &usr)
	if err != nil {
		return middleware.NewRequestError(err, http.StatusUnauthorized)
	}
	resp := map[string]string{
		"token": token,
	}

	return web.Respond(ctx, w, resp, http.StatusOK)
}

// @Summary GetSelf user
// @Security ApiKeyAuth
// @Tags user
// @Description returns yourself
// @Accept  json
// @Produce json
// @Success 200 {integer} integer "httpStatusOk"
// @Failure 500 {object} middleware.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} middleware.ErrorResponse
// @Router /user [get]
func (h *Handler) GetSelf(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	_, err := web.GetValues(ctx)
	if err != nil {
		return web.NewShutdownError("cannot retrieve web values")
	}

	usr := ctx.Value("user").(*user.User)
	if usr == nil {
		return web.NewShutdownError("what the fuck")
	}

	return web.Respond(ctx, w, usr, http.StatusOK)
}

// @Summary GetAllWithFilters user
// @Security ApiKeyAuth
// @Tags user
// @Description returns all users by filters.
// @Accept  json
// @Produce json
// @Param id query string false "user_id"
// @Param email query string false "email"
// @Param company_id query string false "company_id"
// @Param department_id query string false "department_id"
// @Param project_id query string false "project_id"
// @Param team_id query string false "team_id"
// @Param position_id query string false "position_id"
// @Param grade_id query string false "grade_id"
// @Success 200 {integer} integer "httpStatusOk"
// @Failure 500 {object} middleware.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} middleware.ErrorResponse
// @Router /users [get]
func (h *Handler) GetAllWithFilters(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	users, err := h.userService.Fetch(ctx, parseFilters(r))
	if err != nil {
		return middleware.NewRequestError(fmt.Errorf("some shit happened with your request, sorry %w", err), http.StatusBadRequest)
	}

	return web.Respond(ctx, w, users, http.StatusOK)
}

// @Summary UpdateUserProfile user
// @Security ApiKeyAuth
// @Tags user
// @Description updates user profile. Profile contains users private data
// @Accept  json
// @Produce json
// @Param id path string true "user ID"
// @Param input body user.ProfileModel true "profile model"
// @Success 200 {integer} integer "httpStatusOk"
// @Failure 400 {object} middleware.ErrorResponse "httpStatusBadRequest"
// @Failure 401 {object} middleware.ErrorResponse "httpStatusUnauthorized"
// @Failure 500 {object} middleware.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} middleware.ErrorResponse
// @Router /user/{id} [put]
func (h *Handler) UpdateUserProfile(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	id := web.Param(r, "id")

	var rusr user.ProfileModel
	if err := web.Decode(r, &rusr); err != nil {
		return middleware.NewRequestError(fmt.Errorf("bad request : %w", err), http.StatusBadRequest)
	}

	if err := h.userService.UpdateProfile(ctx, &rusr, id); err != nil {
		return middleware.NewRequestError(fmt.Errorf("updating user profile : %w", err), http.StatusBadRequest)
	}

	return web.Respond(ctx, w, nil, http.StatusOK)
}

// @Summary UpdateUserInfo user
// @Security ApiKeyAuth
// @Tags user
// @Description updates user info. Info contains company related info. Can only be accessed by roles: Admin and HR.
// @Accept  json
// @Produce json
// @Param id path string true "user ID"
// @Param input body user.UpdateUserInfo true "user info"
// @Success 200 {integer} integer "httpStatusOk"
// @Failure 400 {object} middleware.ErrorResponse "httpStatusBadRequest"
// @Failure 401 {object} middleware.ErrorResponse "httpStatusUnauthorized"
// @Failure 403 {object} middleware.ErrorResponse "httpStatusForbidden"
// @Failure 500 {object} middleware.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} middleware.ErrorResponse
// @Router /user/info/{id} [put]
func (h *Handler) UpdateUserInfo(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	usr, err := userFromContext(ctx)
	if err != nil {
		return err
	}

	userID := web.Param(r, "id")

	var info user.UpdateUserInfo
	if err = web.Decode(r, &info); err != nil {
		return middleware.NewRequestError(fmt.Errorf("bad request : %w", err), http.StatusBadRequest)
	}

	//TODO : COMPLETE ERROR HANDLING
	if err = h.userService.UpdateInfo(ctx, userID, usr.Info.CompanyID, &info); err != nil {
		switch err {
		case service.ErrNoPermission:
			return middleware.NewRequestError(err, http.StatusForbidden)
		case service.ErrNoUserInfo:
			return middleware.NewRequestError(err, http.StatusBadRequest)
		default:
			return middleware.NewRequestError(fmt.Errorf("updating user info : %w", err), http.StatusBadRequest)
		}
	}

	return web.Respond(ctx, w, nil, http.StatusOK)
}

// @Summary UpdateUserInfoInit user
// @Tags user
// @Description updates user info. Info contains company related info. Used specifically for ORG microservice to set company_id and role without authorization. Normally, to update user use UpdateUserInfo with the right privilleges.
// @Accept  json
// @Produce json
// @Param id path string true "user ID"
// @Param input body user.UpdateUserInfo true "user info"
// @Success 200 {integer} integer "httpStatusOk"
// @Failure 400 {object} middleware.ErrorResponse "httpStatusBadRequest"
// @Failure 401 {object} middleware.ErrorResponse "httpStatusUnauthorized"
// @Failure 500 {object} middleware.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} middleware.ErrorResponse
// @Router /user/info/init/{id} [put]
func (h *Handler) UpdateUserInfoInit(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	userID := web.Param(r, "id")

	var info user.UpdateUserInfo
	if err := web.Decode(r, &info); err != nil {
		return middleware.NewRequestError(fmt.Errorf("bad request : %w", err), http.StatusBadRequest)
	}

	if err := h.userService.UpdateInfoInit(ctx, userID, &info); err != nil {
		return middleware.NewRequestError(err, http.StatusBadRequest)
	}

	return web.Respond(ctx, w, nil, http.StatusOK)
}

// @Summary DeleteUserInfo user
// @Security ApiKeyAuth
// @Tags user
// @Description deletes user info.
// @Accept  json
// @Produce json
// @Param id path string true "user ID"
// @Success 200 {integer} integer "httpStatusOk"
// @Failure 400 {object} middleware.ErrorResponse "httpStatusBadRequest"
// @Failure 401 {object} middleware.ErrorResponse "httpStatusUnauthorized"
// @Failure 500 {object} middleware.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} middleware.ErrorResponse
// @Router /user/info/{id} [delete]
func (h *Handler) DeleteUserInfo(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	usr, err := userFromContext(ctx)
	if err != nil {
		return err
	}

	userID := web.Param(r, "id")
	if userID == "" {
		return middleware.NewRequestError(errors.New("no user id"), http.StatusBadRequest)
	}

	if err := h.userService.DeleteInfo(ctx, userID, usr.Info.CompanyID); err != nil {
		switch err {
		case service.ErrNoPermission:
			return middleware.NewRequestError(err, http.StatusForbidden)
		case service.ErrNoUserInfo:
			return middleware.NewRequestError(err, http.StatusBadRequest)
		default:
			return middleware.NewRequestError(fmt.Errorf("handler : deleting user : %w", err), http.StatusBadRequest)
		}
	}

	return web.Respond(ctx, w, nil, http.StatusOK)
}

// @Summary Create user
// @Tags user
// @Description creates user.
// @Accept  json
// @Produce json
// @Param input body user.Profile true "user profile"
// @Success 200 {integer} integer "httpStatusOk"
// @Failure 400 {object} middleware.ErrorResponse "httpStatusBadRequest"
// @Failure 500 {object} middleware.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} middleware.ErrorResponse
// @Router /user [post]
func (h *Handler) Create(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	_, err := web.GetValues(ctx)
	if err != nil {
		return middleware.NewRequestError(errors.New("web values missing"), http.StatusInternalServerError)
	}

	var usr user.Profile
	if err = web.Decode(r, &usr); err != nil {
		return middleware.NewRequestError(err, http.StatusBadRequest)
	}

	if err = h.userService.Create(ctx, &usr); err != nil {
		return middleware.NewRequestError(err, http.StatusBadRequest)
	}

	return web.Respond(ctx, w, usr, http.StatusCreated)
}

type Token struct {
	Token string `json:"token"`
}

// @Summary Verify user
// @Tags verify
// @Description verifies user by token.
// @Accept  json
// @Produce json
// @Param input body Token true "jwt token"
// @Success 200 {integer} integer "httpStatusOk"
// @Failure 400 {object} middleware.ErrorResponse "httpStatusBadRequest"
// @Failure 500 {object} middleware.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} middleware.ErrorResponse
// @Router /verify [post]
func (h *Handler) Verify(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	token := Token{}
	if err := web.Decode(r, &token); err != nil {
		return middleware.NewRequestError(fmt.Errorf("verifying token : %w", err), http.StatusBadRequest)
	}

	if token.Token == "" {
		return middleware.NewRequestError(errors.New("no token given"), http.StatusBadRequest)
	}

	usr, err := h.auth.Authenticate(ctx, token.Token)
	if err != nil {
		return middleware.NewRequestError(fmt.Errorf("verifiying token : %w", err), http.StatusBadRequest)
	}

	return web.Respond(ctx, w, usr, http.StatusOK)
}

func parseFilters(r *http.Request) *user.Cond {
	queryMap := r.URL.Query()
	res := &user.Cond{
		IDs:         queryMap["id"],
		Email:       queryMap["email"],
		Companies:   queryMap["company_id"],
		Departments: queryMap["department_id"],
		Projects:    queryMap["project_id"],
		Teams:       queryMap["team_id"],
		Positions:   queryMap["position_id"],
		Grades:      queryMap["grade_id"],
	}

	return res
}

func userFromContext(ctx context.Context) (*user.User, error) {
	usr := ctx.Value("user").(*user.User)
	if usr == nil {
		return nil, web.NewShutdownError("no user data in ctx")
	}

	return usr, nil
}
