package handlers

import (
	custom_swagger "gitlab.com/exmatic/auth/pkg/custom-swagger"
	"net/http"
	"os"

	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"

	"gitlab.com/exmatic/auth/cmd/api/handlers/v1/usergrp"
	_ "gitlab.com/exmatic/auth/docs" // calls init
	"gitlab.com/exmatic/auth/internal/core/role"
	"gitlab.com/exmatic/auth/internal/core/user"
	"gitlab.com/exmatic/auth/internal/middleware"
	mongo2 "gitlab.com/exmatic/auth/internal/repository/mongo"
	"gitlab.com/exmatic/auth/internal/service"
	"gitlab.com/exmatic/auth/pkg/web"
)

var version = "v1"

func API(shutdown chan os.Signal, log *zap.SugaredLogger, db *mongo.Database, auth user.Auth) *web.App {
	app := web.NewApp(shutdown, middleware.Logger(log), middleware.Errors(log), middleware.Panics())

	userRepository := mongo2.NewUserRepository(db)
	userService := service.NewUserService(userRepository)

	userHandler := usergrp.NewHandler(auth, userService)
	authen := middleware.Authenticate(auth)

	swaggerHandler := custom_swagger.Handler()
	app.Handle(http.MethodGet, version, "/swagger/*", swaggerHandler)

	app.Handle(http.MethodPost, version, "/user", userHandler.Create)                    //checked
	app.Handle(http.MethodPost, version, "/user/login", userHandler.Login)               //checked
	app.Handle(http.MethodGet, version, "/user/:id", userHandler.GetOne, authen)         //checked
	app.Handle(http.MethodGet, version, "/users", userHandler.GetAllWithFilters, authen) //checked
	app.Handle(http.MethodPut, version, "/user/info/:id", userHandler.UpdateUserInfo, authen, middleware.Authorize(role.Admin, role.HR))
	app.Handle(http.MethodPut, version, "/user/:id", userHandler.UpdateUserProfile, authen) //checked
	app.Handle(http.MethodDelete, version, "/user/info/:id", userHandler.DeleteUserInfo, authen, middleware.Authorize(role.Admin, role.HR))
	app.Handle(http.MethodGet, version, "/user", userHandler.GetSelf, authen)                  //checked
	app.Handle(http.MethodPut, version, "/user/info/init/:id", userHandler.UpdateUserInfoInit) //checked
	app.Handle(http.MethodPost, version, "/verify", userHandler.Verify)                        //checked
	return app
}
